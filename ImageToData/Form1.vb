Public Class Form1
    Public MouseCapture(1, 1) As Integer
    Public RectangleCapture As New Rectangle
    Public TesseractLocation As String = "C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
    Public ImgName, InputFile As String
    Public PointerPadding(1) As Integer
    Public PixelSkipping As Integer = 100
    Public LanguageCode As String = "eng"
    Public PSM As Integer = 7
    Public OEM As Integer = 3
    Public OutPutFile As String = "DateFile.txt"

    '       0   1
    '   0   W1  H1
    '   1   W2  H2
    '
    Function CorrectMouseCapture()
        Dim temp As Integer
        If MouseCapture(0, 0) > MouseCapture(1, 0) Then
            temp = MouseCapture(0, 0)
            MouseCapture(0, 0) = MouseCapture(1, 0)
            MouseCapture(1, 0) = temp
        End If
        If MouseCapture(0, 1) > MouseCapture(1, 1) Then
            temp = MouseCapture(0, 1)
            MouseCapture(0, 1) = MouseCapture(1, 1)
            MouseCapture(1, 1) = temp
        End If
    End Function

    Private Sub AcquireImageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcquireImageToolStripMenuItem.Click


        Try
            Dim CD As New WIA.CommonDialog
            Dim Fetcher As WIA.ImageFile = CD.ShowAcquireImage(WIA.WiaDeviceType.ScannerDeviceType)
            ImgName = "TempImage." + Fetcher.FileExtension
            InputFile = "TempImage." + Fetcher.FileExtension
            If My.Computer.FileSystem.FileExists(ImgName) Then Kill(ImgName)

            Fetcher.SaveFile(ImgName)
            OutPutFile = InputBox("Enter filename for output. Extension will be added automatically.", , OutPutFile) & ".txt"
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        Me.SampleImageBox.ImageLocation = ImgName
        'Me.SampleImageBox.Width = Image.FromFile(ImgName).Width
        'Me.SampleImageBox.Height = Image.FromFile(ImgName).Height

    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ConsoleOutput.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ResetMouseCapture()
        PointerPadding(0) = Me.SampleImageBox.PointToScreen(Me.SampleImageBox.Location).X
        PointerPadding(1) = Me.SampleImageBox.PointToScreen(Me.SampleImageBox.Location).Y
        ConsoleOutput.Show()
    End Sub

    Function ResetMouseCapture()
        MouseCapture(0, 0) = -1
        MouseCapture(0, 1) = -1
        MouseCapture(1, 0) = -1
        MouseCapture(1, 1) = -1
    End Function

    Function SendCapturedAreaToFile()
        Try


            Dim CropRect As New Rectangle(MouseCapture(0, 0), MouseCapture(0, 1), MouseCapture(1, 0) - MouseCapture(0, 0), MouseCapture(1, 1) - MouseCapture(0, 1))
            Dim OriginalImage = Image.FromFile(Me.SampleImageBox.ImageLocation)
            Dim CropImage = New Bitmap(CropRect.Width, CropRect.Height)
            Using grp = Graphics.FromImage(CropImage)
                grp.DrawImage(OriginalImage, New Rectangle(0, 0, CropRect.Width, CropRect.Height), CropRect, GraphicsUnit.Pixel)
                OriginalImage.Dispose()
                CropImage.Save("ForTesseract.png")
                CropImage.dispose()
                ImgName = "ForTesseract.png"
                ImgShrinker(ImgName)
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        End Try
    End Function

    Private Sub FromFileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FromFileToolStripMenuItem.Click
        Try
            Dim myDialog As New OpenFileDialog


            myDialog.ShowDialog()
            ImgName = "TempImage" & My.Computer.FileSystem.GetFileInfo(myDialog.FileName).Extension
            If My.Computer.FileSystem.FileExists(ImgName) Then Kill(ImgName)
            Me.Text = "ImageToData (" & My.Computer.FileSystem.GetName(myDialog.FileName) & ")"
            My.Computer.FileSystem.CopyFile(myDialog.FileName, ImgName)
            InputFile = ImgName
            Me.SampleImageBox.ImageLocation = ImgName
            OutPutFile = My.Computer.FileSystem.GetName(myDialog.FileName) & ".txt"

        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub

    Private Sub SelectTeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectTeToolStripMenuItem.Click
        Try
            Dim myDialog As New OpenFileDialog
            myDialog.ShowDialog()
            TesseractLocation = myDialog.FileName
            Call TesseractCustomCommand("--version")
            Call TesseractCustomCommand("--list-langs")
            
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub

    Function CallTesseractForImage(ByVal ImgLocation As String)
        Dim proc As New Process


        proc.StartInfo.FileName = TesseractLocation
        proc.StartInfo.Arguments = ImgLocation & " stdout " & " -l " & LanguageCode & " --psm " & PSM & " --oem " & OEM
        proc.StartInfo.ErrorDialog = True
        proc.StartInfo.UseShellExecute = False
        proc.StartInfo.RedirectStandardOutput = True
        proc.StartInfo.RedirectStandardError = True
        proc.StartInfo.CreateNoWindow = True
        proc.StartInfo.WorkingDirectory = My.Computer.FileSystem.GetParentPath(TesseractLocation)

        proc.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8

        ConsoleOutput.RichTextBox1.AppendText(Trim(proc.StartInfo.FileName & " " & proc.StartInfo.Arguments & vbCrLf))
        proc.Start()
        'proc.WaitForExit()

        Dim teserror = proc.StandardError.ReadToEnd()
        If Len(teserror) <> 0 Then ConsoleOutput.RichTextBox1.AppendText(teserror & vbCrLf)

        Dim output As String = proc.StandardOutput.ReadToEnd()
        AppendToFile(output)
        ConsoleOutput.RichTextBox1.AppendText(output & vbCrLf)


    End Function

    Function AppendToFile(ByVal TextToWrite As String)
        My.Computer.FileSystem.WriteAllText(OutPutFile, TextToWrite & vbNewLine, True)
    End Function

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Q And e.Control = True Then
            Call CallTesseractForImage(Application.StartupPath & "\" & InputFile)

        End If

        Select Case e.KeyCode
            Case Keys.Up

                'If Me.SampleImageBox.Location.Y < 0 Then Me.SampleImageBox.Location = New System.Drawing.Point(Me.SampleImageBox.Location.X, Me.SampleImageBox.Location.Y + PixelSkipping)
            Case Keys.Down
                'If Math.Abs(Me.SampleImageBox.Location.Y) + Me.Height < Me.SampleImageBox.Image.Height Then Me.SampleImageBox.Location = New System.Drawing.Point(Me.SampleImageBox.Location.X, Me.SampleImageBox.Location.Y - PixelSkipping)
            Case Keys.Left
                'If Me.SampleImageBox.Location.X < 0 Then Me.SampleImageBox.Location = New System.Drawing.Point(Me.SampleImageBox.Location.X + PixelSkipping, Me.SampleImageBox.Location.Y)
            Case Keys.Right
                'If Math.Abs(Me.SampleImageBox.Location.X) + Me.Width < Me.SampleImageBox.Image.Width Then Me.SampleImageBox.Location = New System.Drawing.Point(Me.SampleImageBox.Location.X - PixelSkipping, Me.SampleImageBox.Location.Y)
        End Select

        If (e.Control = True And e.KeyCode = Keys.N) Then AppendToFile(InputBox("Enter text which is to be written to output file:"))

        Select Case e.KeyCode
            Case Keys.W
                Me.Cursor = New Cursor(Cursor.Current.Handle)
                If Not e.Control = True Then Cursor.Position = New Point(Cursor.Position.X, Cursor.Position.Y - 25) Else Cursor.Position = New Point(Cursor.Position.X, Cursor.Position.Y - 1)
            Case Keys.S
                Me.Cursor = New Cursor(Cursor.Current.Handle)
                If Not e.Control = True Then Cursor.Position = New Point(Cursor.Position.X, Cursor.Position.Y + 25) Else Cursor.Position = New Point(Cursor.Position.X, Cursor.Position.Y + 1)
            Case Keys.A
                Me.Cursor = New Cursor(Cursor.Current.Handle)
                If Not e.Control = True Then Cursor.Position = New Point(Cursor.Position.X - 25, Cursor.Position.Y) Else Cursor.Position = New Point(Cursor.Position.X - 1, Cursor.Position.Y)
            Case Keys.D
                Me.Cursor = New Cursor(Cursor.Current.Handle)
                If Not e.Control = True Then Cursor.Position = New Point(Cursor.Position.X + 25, Cursor.Position.Y) Else Cursor.Position = New Point(Cursor.Position.X + 1, Cursor.Position.Y)
        End Select

        If e.KeyCode = Keys.ShiftKey Then
            Me.Cursor = New Cursor(Cursor.Current.Handle)
            Call SampleImageBox_MouseDown(Me, New System.Windows.Forms.MouseEventArgs(Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0))
        End If
    End Sub

    Private Sub SetLanguageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetLanguageToolStripMenuItem.Click
        LanguageCode = InputBox("Enter language code supported by Tesserect.", , LanguageCode)
    End Sub

    Private Sub SampleImageBox_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SampleImageBox.MouseDown
        MouseCapture(0, 0) = Control.MousePosition.X - Me.SampleImageBox.Location.X - PointerPadding(0)
        MouseCapture(0, 1) = Control.MousePosition.Y - Me.SampleImageBox.Location.Y - PointerPadding(1)
    End Sub

    Private Sub SampleImageBox_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles SampleImageBox.MouseHover
        Me.PositionLabel.Visible = True
    End Sub

    Private Sub SampleImageBox_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles SampleImageBox.MouseLeave
        Me.PositionLabel.Visible = False
    End Sub

    Private Sub SampleImageBox_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SampleImageBox.MouseMove

        Me.PositionLabel.Text = (Control.MousePosition.X - Me.SampleImageBox.Location.X - PointerPadding(0)) & " X " & (Control.MousePosition.Y - Me.SampleImageBox.Location.Y - PointerPadding(1))
        Me.PositionLabel.Location = New System.Drawing.Point(PointToScreen(New System.Drawing.Point(e.Location.X + 10 - Math.Abs(Me.SampleImageBox.Location.X), e.Location.Y + 10 - Math.Abs(Me.SampleImageBox.Location.Y))).X, PointToScreen(New System.Drawing.Point(e.Location.X + 10 - Math.Abs(Me.SampleImageBox.Location.X), e.Location.Y + 10 - Math.Abs(Me.SampleImageBox.Location.Y))).Y)

        If MouseCapture(0, 0) <> -1 And MouseCapture(0, 1) <> 0 Then
            Me.Refresh()

            MouseCapture(1, 0) = Control.MousePosition.X - Me.SampleImageBox.Location.X - PointerPadding(0)
            MouseCapture(1, 1) = Control.MousePosition.Y - Me.SampleImageBox.Location.Y - PointerPadding(1)

            Me.SampleImageBox.CreateGraphics().DrawRectangle(Pens.Red, MouseCapture(0, 0), MouseCapture(0, 1), MouseCapture(1, 0) - MouseCapture(0, 0), MouseCapture(1, 1) - MouseCapture(0, 1))
        End If
    End Sub

    Private Sub SampleImageBox_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SampleImageBox.MouseUp
        MouseCapture(1, 0) = Control.MousePosition.X - Me.SampleImageBox.Location.X - PointerPadding(0)
        MouseCapture(1, 1) = Control.MousePosition.Y - Me.SampleImageBox.Location.Y - PointerPadding(1)

        Call CorrectMouseCapture()
        Me.SampleImageBox.CreateGraphics().DrawRectangle(Pens.Red, MouseCapture(0, 0), MouseCapture(0, 1), MouseCapture(1, 0) - MouseCapture(0, 0), MouseCapture(1, 1) - MouseCapture(0, 1))


        Call SendCapturedAreaToFile()
        Call ResetMouseCapture()
        Call CallTesseractForImage(Application.StartupPath & "\" & ImgName)
    End Sub

    Function TesseractCustomCommand(ByVal TesseractCmd As String)
        Dim proc As New Process

        proc.StartInfo.FileName = TesseractLocation
        proc.StartInfo.Arguments = " " & TesseractCmd
        proc.StartInfo.ErrorDialog = True
        proc.StartInfo.UseShellExecute = False
        proc.StartInfo.RedirectStandardOutput = True
        proc.StartInfo.RedirectStandardError = True
        proc.StartInfo.CreateNoWindow = True
        proc.StartInfo.WorkingDirectory = My.Computer.FileSystem.GetParentPath(TesseractLocation)

        ConsoleOutput.RichTextBox1.AppendText(proc.StartInfo.FileName & " " & proc.StartInfo.Arguments & vbCrLf)
        proc.Start()
        'proc.WaitForExit()

        Dim teserror = proc.StandardError.ReadToEnd()
        If Len(teserror) <> 0 Then ConsoleOutput.RichTextBox1.AppendText(teserror & vbCrLf)

        Dim output As String = proc.StandardOutput.ReadToEnd()
        ConsoleOutput.RichTextBox1.AppendText(output & vbCrLf)

    End Function

    Private Sub PageSegmentationModeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageSegmentationModeToolStripMenuItem.Click
        PSM = InputBox("Select Page Segmentation Mode:" & vbCrLf & "   0    Orientation and script detection (OSD) only." & vbCrLf & "  1    Automatic page segmentation with OSD." & vbCrLf & "  2    Automatic page segmentation, but no OSD, or OCR." & vbCrLf & "  3    Fully automatic page segmentation, but no OSD. (Default)" & vbCrLf & "  4    Assume a single column of text of variable sizes." & vbCrLf & "  5    Assume a single uniform block of vertically aligned text." & vbCrLf & "  6    Assume a single uniform block of text." & vbCrLf & "  7    Treat the image as a single text line." & vbCrLf & "  8    Treat the image as a single word." & vbCrLf & "  9    Treat the image as a single word in a circle." & vbCrLf & " 10    Treat the image as a single character." & vbCrLf & " 11    Sparse text. Find as much text as possible in no particular order." & vbCrLf & " 12    Sparse text with OSD." & vbCrLf & " 13    Raw line. Treat the image as a single text line,       bypassing hacks that are Tesseract-specific.", , PSM)
    End Sub

    Private Sub OCREngineModeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OCREngineModeToolStripMenuItem.Click
        OEM = InputBox("  0    Legacy engine only." & vbCrLf & "   1    Neural nets LSTM engine only." & vbCrLf & "  2    Legacy + LSTM engines." & vbCrLf & "  3    Default, based on what is available.", , OEM)
    End Sub

    Private Sub Form1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.ShiftKey Then
            Me.Cursor = New Cursor(Cursor.Current.Handle)
            Call SampleImageBox_MouseUp(Me, New System.Windows.Forms.MouseEventArgs(Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0))
        End If
    End Sub

    Function ImgShrinker(ByVal ImgLocation As String) As Boolean
        Exit Function
        ConsoleOutput.RichTextBox1.AppendText("Using Shrinker" & vbCrLf)
        On Error GoTo -1
        Dim sx, sy, px, py, x, y As Integer
        Dim pixelcolor As Color
        'Try
        'Dim CropRect As New Rectangle(MouseCapture(0, 0), MouseCapture(0, 1), MouseCapture(1, 0) - MouseCapture(0, 0), MouseCapture(1, 1) - MouseCapture(0, 1))

        Dim OriginalImage As System.Drawing.Bitmap = Image.FromFile(ImgLocation)
        px = OriginalImage.Width
        py = OriginalImage.Height
        ConsoleOutput.RichTextBox1.AppendText("Size Before Shrinking: x=" & px & " y=" & py & vbCrLf)
        If (OriginalImage.Width Mod 2) = 1 Then sx = (OriginalImage.Width - 1) / 2 Else sx = (OriginalImage.Width) / 2
        If (OriginalImage.Height Mod 2) = 1 Then sy = (OriginalImage.Height - 1) / 2 Else sy = (OriginalImage.Height) / 2
        Dim CropImage As System.Drawing.Bitmap = New Bitmap(sx + 1, sy + 1)


        For y = 1 To sy
            For x = 1 To sx
                pixelcolor = OriginalImage.GetPixel(x * 2, y * 2)
                CropImage.SetPixel(x, y, pixelcolor)
            Next x
        Next y

        ConsoleOutput.RichTextBox1.AppendText("Size After Shrinker: x=" & sx & " y=" & sy & vbCrLf)
        Dim grp As System.Drawing.Graphics = Graphics.FromImage(CropImage)
        grp.DrawImage(CropImage, 0, 0)
        OriginalImage.Dispose()
        CropImage.Save(ImgLocation)
        CropImage.Dispose()
        'Using grp = Graphics.FromImage(CropImage)
        'grp.DrawImage(OriginalImage, New Rectangle(0, 0, CropRect.Width, CropRect.Height), CropRect, GraphicsUnit.Pixel)
        'OriginalImage.Dispose()
        'CropImage.Save("ForTesseract.png")
        'ImgName = "ForTesseract.png"
        'End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    Exit Function
        'End Try
    End Function
End Class
